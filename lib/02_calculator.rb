def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(numbers)
  result = 0
  i = 0
  while i < numbers.length
    result += numbers[i]
    i += 1
  end
  result
end
