def echo(greeting)
  greeting
end

def shout(greeting)
  greeting.upcase
end

def repeat(word, num_of_repeats = 2)
  repeated = "#{word}"
  (num_of_repeats - 1).times {repeated += " #{word}"}
  repeated
end

def start_of_word(word, number_of_letters = 1)
  word[0..number_of_letters - 1]
end

def first_word(words)
  array_of_words = words.split(" ")
  array_of_words[0]
end

def titleize(title)
  array_of_words = title.split(" ")
  array_of_words[0].capitalize!
  array_of_words.last.capitalize!
  i = 0
  while i < array_of_words.length
    if array_of_words[i].length > 4
      array_of_words[i].capitalize!
    end
    i += 1
  end
  array_of_words.join(" ")
end
