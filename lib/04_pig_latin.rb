def has_qu?(word)
  word[0..1] == "qu"
end

def vowel_start?(word)
  first_letter = word[0].downcase

  puts word
  if /[^aeiou]/.match(first_letter)
    if has_qu?(word)
      return vowel_start?("#{word[2..-1]}#{word[0..1]}")
    else
      return vowel_start?("#{word[1..-1]}#{word[0]}")
    end
  end
  return word
end

def translate(not_pig_latin)
  pig_latin = []
  not_pig_latin.split(" ").collect do | string |
    pig_latin << vowel_start?(string) + "ay"
  end
  pig_latin.join(" ")
end
